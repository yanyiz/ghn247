import React, { Component } from 'react';
import {
  Router,
  Stack,
  Scene
} from 'react-native-router-flux'
import Login from './src/container/Login'
import Register from './src/container/Register'
import Main from './src/container/Main'
import AddPackage from './src/container/AddPackage'
import ForgetPassword from './src/container/ForgetPassword'
import Search from './src/component/search/Search'

class Routers extends Component {
  render() {
    return (
      <Router>
        <Stack key="root" hideNavBar={true}>
          <Scene
            key="login"
            component={Login}
            title="Login"
            initial
          />
          <Scene
            key="register"
            component={Register}
            title="Register"
          />
          <Scene
            key="forgotpassword"
            component={ForgetPassword}
            title="ForgetPassword"
          />
          <Scene
            key="main"
            component={Main}
            title="Main"
          />
          <Scene
            key="addpackage"
            component={AddPackage}
            title="AddPackage"
          />
          <Scene
            key="search"
            component={Search}
            title="Search"
          />
        </Stack>
      </Router>
    );
  }
}

export default Routers;
