import React, { Component } from 'react';
import { View, Text,  } from 'react-native';
import styled from 'styled-components';
import {
  Container,
  Content
} from "native-base";
import HeaderMain from '../component/general/Header'
import FabComponent from '../component/general/FabComponent'
import InputComponent from '../component/package/InputComponent'

class Main extends Component {
  render() {
    return (
      <Container>
        <HeaderMain title='Thêm Package'/>
        <InputComponent/>
        <Content/>
        <FabComponent/>
      </Container>
    );
  }
}

export default Main;

// const ContentView = styled(Content)`
//   height:100%
// `;
