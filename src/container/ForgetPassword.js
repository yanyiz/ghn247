import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image
} from 'react-native';
import {
  Container,
  Content
} from 'native-base';
import styled from 'styled-components';
import { Actions } from 'react-native-router-flux'
import InputComponent from '../component/forgotpasword/InputComponent'

class ForgetPassword extends Component {
  render() {
    const imgLogo = require('../common/img/247.png');
    return (
      <ContainerView>
        <Scroll>
          {/* Logo */}
          <LogoView>
            <Logo source={imgLogo} />
          </LogoView>

          {/* forgot Form */}
          <MainView >
            <InputComponent />
          </MainView>

          {/* space for avoid keyboard */}
          <View style={{ height: 10 }}></View>

        </Scroll>
      </ContainerView>
    );
  }
}

export default ForgetPassword;

const ContainerView = styled(Container)`
  background-color: white;
`;

const Scroll = styled(ScrollView)`
  width: 100%;
`;

const MainView = styled(View)`
  flex: 1;
  width: 100%;
  padding-left: 15px;
  padding-right: 15px;
`;

const LogoView = styled(View)`
  flex: 1;
  width: 100%;
  padding-left: 15px;
  padding-right: 15px;
  margin-top: 15px;
  margin-bottom: 10px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

const Logo = styled(Image)`
  background-color: white;
  width: 100%; 
  height: 230px;
`;