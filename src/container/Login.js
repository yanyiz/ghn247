import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image

} from 'react-native';
import {
  Container,
  Content
} from 'native-base';
import styled from 'styled-components';
import { Actions } from 'react-native-router-flux'
import InputComponent from '../component/login/InputComponent'

class Login extends Component {
  render() {
    const imgLogo = require('../common/img/247.png');
    return (
      <ContainerView>
        <Content>
          {/* Logo */}
          <LogoView>
            <Logo source={imgLogo} />
          </LogoView>

          <MainView>
            {/* Input form */}
            <InputComponent />
            {/* AnchorLink */}
            <View>
              <TouchableOpacity
                onPress={() => {
                  Actions.register()
                }}
              >
                <ForgotPasswordText >
                  Chưa có tài khoản?
                  <AnchorLink>
                    Tạo tài khoản!
                  </AnchorLink>
                </ForgotPasswordText>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() => {
                Actions.forgotpassword()
              }}
            >
              <ForgotPasswordText >
                Quên mật khẩu?
              <AnchorLink>
                  Click vào đây
              </AnchorLink>
              </ForgotPasswordText>
            </TouchableOpacity>
          </MainView>
        </Content>
      </ContainerView>
    );
  }
}

export default Login;

const ContainerView = styled(Container)`
  background-color: white;
`;

const LogoView = styled(View)`
  flex: 1;
  width: 100%;
  padding-left: 15px;
  padding-right: 15px;
  margin-top: 15px;
  margin-bottom: 10px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

const Logo = styled(Image)`
  background-color: white;
  width: 100%; 
  height: 230px;
`;

const MainView = styled(View)`
  flex: 1;
  width: 100%;
  padding: 15px;
`;

const ForgotPasswordText = styled(Text)`
  color: #89C06A;
  font-size: 14px;
  font-style: italic;
  margin-top: 10px;
  width: 100%;
  text-align: center;
`
const AnchorLink = styled(Text)`
  color: black;
`