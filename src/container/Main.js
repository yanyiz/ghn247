import React, { Component } from 'react';
import { View, Text,  } from 'react-native';
import styled from 'styled-components';
import {
  Container,
  Content
} from "native-base";
import HeaderMain from '../component/general/Header'
import FabComponent from '../component/general/FabComponent'
import ListComponent from '../component/package/ListComponent'

class Main extends Component {
  render() {
    return (
      <Container>
        <HeaderMain title='GHN247'/>
        <ListComponent/>
        <Content/>
        <FabComponent/>
        {/* <FooterMain/> */}
      </Container>
    );
  }
}

export default Main;

const ContainerView = styled(Container)`

`;
