import React, {
    Component,
    Fragment,
    StrictMode,
} from 'react';
import {
    View,
    Button,
} from 'react-native';
import {
    Form, Item, Input, Label, Icon
} from 'native-base';
import styled from 'styled-components';
//- create context
import { InputContext } from '../../context/LoginContext'
import ButtonComponent from './ButtonComponent'

class InputComponent extends Component {

    state = {
        email: '',
        password: ''
    }

    constructor(props) {
        super(props)
    }

    handlePress = () => {
        // login(this.state.email, this.state.password)
        // alert('email: ' + this.state.email + 'password: ' + this.state.password)
    }

    render() {
        return (
            <Fragment>
                <Form>
                    <ItemForm rounded success >
                        <Icon name='mail' style={{color:'#4CAF50'}} />
                        <InputForm
                            placeholder='Email'
                            onChangeText={(email) => this.setState({ email })}
                        />
                    </ItemForm>
                    <ItemForm rounded success >
                        <Icon name='lock' style={{color:'#4CAF50'}}/>
                        <InputForm
                            placeholder='Mật Khẩu'
                            secureTextEntry
                            onChangeText={(password) => this.setState({ password })}
                        />
                    </ItemForm>
                </Form>
                <ButtonComponent email={this.state.email} password={this.state.password} />


            </Fragment>
        );
    }
}

export default InputComponent;

// Css
const ItemForm = styled(Item)`
    background-color:white;
    shadow-color: #000;
    shadow-offset: { width: 1, height: 2 };
    shadow-opacity: 1;
    shadow-radius: 0;
    elevation: 2;
    margin-bottom:10;
    border-color: #4CAF50;
`;
const InputForm = styled(Input)`
    font-size: 14px;
`;