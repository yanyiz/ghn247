import React, {
    Component,
    Fragment,
    StrictMode,
} from 'react';
import {
    View,
    Button,
} from 'react-native';
import {
    Form, Item, Input, Label, Icon, Text
} from 'native-base';
import styled from 'styled-components';
//- create context
import ButtonComponent from './ButtonComponent'

class InputComponent extends Component {

    state = {
        email: '',
    }

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Fragment>
                <Margin>
                    <Title>Vui lòng nhập địa chỉ mail đã đăng ký</Title>
                    <Form>
                        <ItemForm rounded success >
                            <Icon name='mail' style={{ color: '#4CAF50' }} />
                            <InputForm
                                placeholder='Email'
                                onChangeText={(email) => this.setState({ email })}
                            />
                        </ItemForm>
                    </Form>
                    <ButtonComponent email={this.state.email} />
                </Margin>
            </Fragment>
        );
    }
}

export default InputComponent;

// Css
const ItemForm = styled(Item)`
    background-color:white;
    shadow-color: #000;
    shadow-offset: { width: 1, height: 2 };
    shadow-opacity: 1;
    shadow-radius: 0;
    elevation: 2;
    margin-bottom:10;
    border-color: #4CAF50;
`;
const InputForm = styled(Input)`
    font-size: 14px;
`;

const Margin = styled(View)`
    margin-top: 20px;
`;

const Title = styled(Text)`
    text-align: center;
    margin-bottom: 10px;
    color: #4CAF50;
`;