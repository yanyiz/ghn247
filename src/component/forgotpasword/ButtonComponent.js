import React, {
  Component,
  Fragment
} from 'react';
import {
  View,
  Animated,
  TouchableOpacity,
  StyleSheet,
  Alert,
  AsyncStorage 
} from 'react-native';
import { Text } from 'native-base';
import styled from 'styled-components';
import { Actions } from 'react-native-router-flux'

class ButtonComponent extends Component {

  constructor(props) {

    super(props)
    this.state = {
      pressStatus: false
    }

  }

  _onHideUnderlay() {
    this.setState({ pressStatus: false });
  }
  _onShowUnderlay() {
    this.setState({ pressStatus: true });
  }

  handlePress = () => {
    // login(this.props.email, this.props.password)
    alert('Password mới đã được gởi tới mail của bạn')

  }
  render() {
    return (
      <Fragment>
        <Login onPress={this.handlePress}>
          <StyledView>
            Lấy Password mới
          </StyledView>
        </Login>
      </Fragment>
    );
  }
}

export default ButtonComponent;


login = (_email, _password) => {
  fetch('http://54.199.238.117:3001/user/login', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email: _email,
      password: _password,
    }),
  }).then((response) => response.text())
    .then((responseText) => {
      // chuyển hương
      let data = JSON.parse(responseText);

      if (!data.data) {
        Alert.alert(
          'Lỗi',
          'Tài khoản hoặc mật khẩu không đúng!',
          [
            { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
          { cancelable: false }
        )
      } else {
        AsyncStorage.setItem('userID', data.data._id);
        Actions.main();
      }

    })
    .catch((error) => {
      console.error(error);
    });
}

// Css
const StyledView = styled.Text`
  font-size: 14px;
  text-align: center;
  color: white;
  padding-left:15px;
  padding-right:15px;
`;
const Login = styled.TouchableOpacity`
  background-color: #4CAF50;
  box-shadow: 0 5px 10px #000000;
  margin-top:10;
  justify-content: center;
  height:45px;
  border-radius:25px;
  elevation: 2
`;