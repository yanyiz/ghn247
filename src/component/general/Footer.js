import React, { Component } from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Footer,
    FooterTab,
    Text,
    Body,
    Left,
    Right,
    Icon,
    StyleProvider
} from "native-base";
import { StyleSheet } from 'react-native'
import styled from 'styled-components';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';



class FooterMain extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tab1: false,
            tab2: false,
            tab3: true,
            tab4: false,
            tab5: false
        };
    }
    toggleTab1() {
        this.setState({
            tab1: true,
            tab2: false,
            tab3: false,
            tab4: false,
            tab5: false
        });
    }
    toggleTab2() {
        this.setState({
            tab1: false,
            tab2: true,
            tab3: false,
            tab4: false,
            tab5: false
        });
    }
    toggleTab3() {
        this.setState({
            tab1: false,
            tab2: false,
            tab3: true,
            tab4: false,
            tab5: false
        });
    }
    toggleTab4() {
        this.setState({
            tab1: false,
            tab2: false,
            tab3: false,
            tab4: true,
            tab5: false
        });
    }
    addHandlePress = () => {
        alert('You press button add')
    }
    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Footer>
                    <FooterTabView>
                        <Button vertical active={this.state.tab1} onPress={() => this.toggleTab1()}>
                            <Icon active={this.state.tab1} name="home" />
                            <Text></Text>
                        </Button>
                        <Button vertical active={this.state.tab2} onPress={() => this.toggleTab2()}>
                            <Icon active={this.state.tab2} name="camera" />
                            <Text></Text>
                        </Button>
                        <ButtonView onPress={this.addHandlePress}>
                            <Icon name="add" />
                            {/* <Text>Camera</Text> */}
                        </ButtonView>
                        <Button vertical active={this.state.tab3} onPress={() => this.toggleTab3()}>
                            <Icon active={this.state.tab3} name="wallet" />
                            <Text></Text>
                        </Button>
                        <Button vertical active={this.state.tab4} onPress={() => this.toggleTab4()}>
                            <Icon active={this.state.tab4} name="menu" />
                            <Text></Text>
                        </Button>
                    </FooterTabView>
                </Footer>
            </StyleProvider>

        );
    }
}

export default FooterMain;

const FooterTabView = styled(FooterTab)`
    background-color: #4CAF50;
`;
// const TextView = styled(Text)`
//     font-size: 10px
// `;
const ButtonView = styled(Button)`
    font-size: 14px
`;

