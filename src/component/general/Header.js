import React, { Component } from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  StyleProvider
} from "native-base";
import { StyleSheet } from 'react-native'
import styled from 'styled-components';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import { Actions } from 'react-native-router-flux'

class HeaderMain extends Component {
  constructor(props) {

    super(props)
    this.state = {
      pressStatus: false
    }

  }

  handlePress = () => {
    Actions.search()
  }
  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <HeaderView>
          {/* <Left>
            <Button transparent onPress={this.handlePress}>
              <Icon name="menu" />
            </Button>
          </Left> */}
          <Body>
            <TitleView>{this.props.title}</TitleView>
          </Body>
          <Right>
            <Button transparent onPress={this.handlePress}>
              <Icon name="search" />
            </Button>
            <Button transparent>
              <Icon name="more" />
            </Button>
          </Right>
        </HeaderView>
      </StyleProvider>

    );
  }
}

export default HeaderMain;

const HeaderView = styled(Header)`
  background-color: #4CAF50;
`;

const TitleView = styled(Title)`

`;