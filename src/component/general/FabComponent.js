import React, {
    Component,
    Fragment,
    StrictMode,
} from 'react';
import {
    Container, Header, View, Button, Icon, Fab,IconNB
} from 'native-base';
import styled from 'styled-components';
import { Actions } from 'react-native-router-flux'


class FabComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            active: false
          };

    }

    addPress = () => {
        Actions.addpackage();
    }
    listPress = () => {
        Actions.main();
    }
    render() {
        return (
            <Fragment>
                <Container>
                    <View style={{ flex: 1 }}>
                        {/* <Fab
                            direction="up"
                            containerStyle={{}}
                            style={{ backgroundColor: '#4CAF50' }}
                            position="bottomRight"
                            onPress={this.handlePress}>
                            <Icon name="add" />
                        </Fab> */}
                        <Fab
                            active={this.state.active}
                            direction="left"
                            containerStyle={{}}
                            style={{ backgroundColor: "#4CAF50" }}
                            position="bottomRight"
                            onPress={() => this.setState({ active: !this.state.active })}
                        >    
                            <IconNB name="md-share" />
                            <Button style={{ backgroundColor: '#3B5998' }} onPress={this.addPress}>
                                <Icon name="add" />
                            </Button>
                            <Button style={{ backgroundColor: "#ea9013" }} onPress={this.listPress}>
                                <IconNB name="md-list-box" />
                            </Button>
                        </Fab>
                    </View>
                </Container>
            </Fragment>
        );
    }
}

export default FabComponent;

// Css
// const ItemForm = styled(Item)`

// `;
