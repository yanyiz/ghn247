import React, { Component } from 'react';
import { View } from 'react-native';
import styled from 'styled-components';
import {
    Container,
    Header,
    Button,
    Icon,
    Item,
    Input,
    Content,
    Text,
    Right,
    Body
} from "native-base";
import FabComponent from '../general/FabComponent'


class Search extends Component {
    render() {
        return (
            <Container>
                <HeaderView searchBar rounded>
                    {/* <Body> */}
                    <Item>
                        <Icon active name="search" />
                        <Input placeholder="Search" />
                        <ButtonView transparent>
                            <TextView>Search</TextView>
                        </ButtonView>
                    </Item>
                </HeaderView>

                <Content padder>
                    {/* <Button
                        block
                        onPress={() => this.props.navigation.navigate("DrawerOpen")}
                    >
                        <Text>Back</Text>
                    </Button> */}
                </Content>
                <FabComponent />
            </Container>
        );
    }
}

export default Search;

const HeaderView = styled(Header)`
  background-color: #4CAF50;
`;
const ButtonView = styled(Button)`
    background-color: #4CAF50;
`;
const TextView = styled(Text)`
    color: white;
`;

