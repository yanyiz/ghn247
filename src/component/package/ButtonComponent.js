import React, {
  Component,
  Fragment
} from 'react';
import {
  View,
  Animated,
  TouchableHighlight,
  TouchableOpacity,
  StyleSheet,
  Alert,
  AsyncStorage
} from 'react-native';
import { Text } from 'native-base';
import styled from 'styled-components';
import { Actions } from 'react-native-router-flux'
import { InputContext } from '../../context/LoginContext'

class ButtonComponent extends Component {

  constructor(props) {

    super(props)
    this.state = {
      pressStatus: false,
      latitude: null,
      longitude: null,
      error: null,
      userID : null
    }

  }

  componentDidMount() {
    // AsyncStorage.getItem('userID').then((value) => alert(value));

    AsyncStorage.getItem('userID').then((value) => this.setState({ userID: value }));
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );

  }

  _onHideUnderlay() {
    this.setState({ pressStatus: false });
  }
  _onShowUnderlay() {
    this.setState({ pressStatus: true });
  }

  handlePress = () => {
    // alert(this.props.reciverName + this.props.reciverTel)
    addPackage(this.state.userID, this.props.reciverName, this.props.reciverTel,
                this.props.reciverAddr, this.props.deliveryAddr,
                this.props.cod, this.props.weight, this.props.selectType,
                this.props.note, this.props.area , this.state.latitude,  this.state.longitude
              )
  }
  render() {
    return (
      <Fragment>
        {/* <View style={{ flexGrow: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text>Latitude: {this.state.latitude}</Text>
          <Text>Longitude: {this.state.longitude}</Text>
          {this.state.error ? <Text>Error: {this.state.error}</Text> : null}
        </View> */}
        <Button onPress={this.handlePress}>
          <StyledView>Tạo package</StyledView>
        </Button>
      </Fragment>
    );
  }
}

export default ButtonComponent;

const styles = StyleSheet.create({
  button: {
    borderColor: '#000066',
    borderWidth: 1,
    borderRadius: 10,
  },
  buttonPress: {
    borderColor: '#000066',
    // backgroundColor: '#000066',
    backgroundColor: 'black',
    borderWidth: 1,
    borderRadius: 10,
  },
})

addPackage = (_userID, _reciverName, _reciverTel, _reciverAddr, _deliveryAddr, _cod, _weight, _selectType, _note, _area ,_latitude, _longitude) => {
  fetch('http://54.199.238.117:3001/package/create', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      reciverName: _reciverName,
      reciverTel: _reciverTel,
      reciverAddr: _reciverAddr,
      deliveryAddr: _deliveryAddr,
      cod: _cod,
      weight: _weight,
      selectType: _selectType,
      note: _note,
      area: _area,
      geo: JSON.stringify([_latitude, _longitude]),
      userID: _userID
    }),
  }).then((response) => response.text())
    .then((responseText) => {
      let data = JSON.parse(responseText);
      if (!data.data) {
        Alert.alert(
          'Lỗi',
          data.message,
          [
            { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
          { cancelable: false }
        )
      } else {
        Alert.alert(
          'Thành công!!!',
          data.message,
          [
            { text: 'OK', onPress: () => Actions.login() },
          ],
          { cancelable: false }
        )
      }

    })
    .catch((error) => {
      console.error(error);
    });

}

// Css
const StyledView = styled.Text`
    font-size: 14px;
    text-align: center;
    color: white;
    padding-left:15px;
    padding-right:15px;
  `;
const Button = styled.TouchableOpacity`
    background-color: #4CAF50;
    box-shadow: 0 5px 10px #000000;
    margin-top:5px;
    justify-content: center;
    height:45px;
    border-radius:25px;
    elevation: 2
  `;