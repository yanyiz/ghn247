import React, {
    Component,
    Fragment,
    StrictMode,
} from 'react';
import { View, TouchableOpacity, ScrollView } from 'react-native';
import styled from 'styled-components';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';


class ListComponent extends Component {
    render() {
        const imgLogo = require('../../common/img/247.png');
        return (
            <Fragment>
                <ScrollViewNew>
                    <Card style={{ flex: 0 }}>
                        <CardItem>
                            <Left>
                                <Thumbnail source={imgLogo} />
                                <Body>
                                    <TouchableOpacity
                                        onPress={() => {
                                            alert("View Deatils")
                                        }}
                                    >
                                        <TitleLink>f4bd1326c7</TitleLink>
                                    </TouchableOpacity>
                                    <Text note>Quận 1</Text>
                                </Body>
                            </Left>
                             {/* <Left>
                                <Service note>Dịch vụ 24H</Service>
                            </Left> */}
                            <Right>
                                <Service note>Dịch vụ 24H</Service>
                                {/* <Text>11h Trước</Text> */}
                            </Right>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Text>Đến: Bảo bảo</Text>
                                <Text>Khối lượng: 5g</Text>
                                <Text>Lưu ý: hàng dễ vỡ xin nhẹ tay</Text>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Body>
                                {/* <Icon active name="chatbubbles" /> */}
                                <Text>Trạng thái: </Text>
                                <Status note>Chưa tiếp nhận</Status>
                            </Body>
                            <Right>
                                <Text>11h Trước</Text>
                            </Right>
                        </CardItem>
                    </Card>
                    {/* card 2 */}
                    <Card style={{ flex: 0 }}>
                        <CardItem>
                            <Left>
                                <Thumbnail source={imgLogo} />
                                <Body>
                                    <TouchableOpacity
                                        onPress={() => {
                                            alert("View Deatils")
                                        }}
                                    >
                                        <TitleLink>5bđ42159e</TitleLink>
                                    </TouchableOpacity>
                                    <Text note>Quận 2</Text>
                                </Body>
                            </Left>
                             {/* <Left>
                                <Service note>Dịch vụ 24H</Service>
                            </Left> */}
                            <Right>
                                <Service note>Dịch vụ 24H</Service>
                                {/* <Text>11h Trước</Text> */}
                            </Right>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Text>Đến: Bảo bảo 2</Text>
                                <Text>Khối lượng: 15g</Text>
                                <Text>Lưu ý: hàng nhạy cảm có thể xem</Text>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Body>
                                {/* <Icon active name="chatbubbles" /> */}
                                <Text>Trạng thái: </Text>
                                <Status note>Chưa tiếp nhận</Status>
                            </Body>
                            <Right>
                                <Text>11h Trước</Text>
                            </Right>
                        </CardItem>
                    </Card>
                    {/* card 3 */}
                    <Card style={{ flex: 0 }}>
                        <CardItem>
                            <Left>
                                <Thumbnail source={imgLogo} />
                                <Body>
                                    <TouchableOpacity
                                        onPress={() => {
                                            alert("View Deatils")
                                        }}
                                    >
                                        <TitleLink>7975d035f5</TitleLink>
                                    </TouchableOpacity>
                                    <Text note>Quận 3</Text>
                                </Body>
                            </Left>
                             {/* <Left>
                                <Service note>Dịch vụ 24H</Service>
                            </Left> */}
                            <Right>
                                <Service note>Dịch vụ 24H</Service>
                                {/* <Text>11h Trước</Text> */}
                            </Right>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Text>Đến: Bảo bảo</Text>
                                <Text>Khối lượng: 5g</Text>
                                <Text>Lưu ý: hàng hot xinh không xem</Text>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Body>
                                {/* <Icon active name="chatbubbles" /> */}
                                <Text>Trạng thái: </Text>
                                <Status note>Chưa tiếp nhận</Status>
                            </Body>
                            <Right>
                                <Text>11h Trước</Text>
                            </Right>
                        </CardItem>
                    </Card>
                    {/* card 4 */}
                    <Card style={{ flex: 0 }}>
                        <CardItem>
                            <Left>
                                <Thumbnail source={imgLogo} />
                                <Body>
                                    <TouchableOpacity
                                        onPress={() => {
                                            alert("View Deatils")
                                        }}
                                    >
                                        <TitleLink>7975d035f5</TitleLink>
                                    </TouchableOpacity>
                                    <Text note>Quận 3</Text>
                                </Body>
                            </Left>
                             {/* <Left>
                                <Service note>Dịch vụ 24H</Service>
                            </Left> */}
                            <Right>
                                <Service note>Dịch vụ 24H</Service>
                                {/* <Text>11h Trước</Text> */}
                            </Right>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Text>Đến: Bảo bảo</Text>
                                <Text>Khối lượng: 5g</Text>
                                <Text>Lưu ý: hàng hot xinh không xem</Text>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Body>
                                {/* <Icon active name="chatbubbles" /> */}
                                <Text>Trạng thái: </Text>
                                <Status note>Chưa tiếp nhận</Status>
                            </Body>
                            <Right>
                                <Text>11h Trước</Text>
                            </Right>
                        </CardItem>
                    </Card>
                    <View style={{height:30}}/>
                </ScrollViewNew>
            </Fragment>
        );
    }
}

export default ListComponent;

const ScrollViewNew = styled(ScrollView)`
    padding: 15px;
`;
const TitleLink = styled(Text)`
    color: #0a813d;
`;
const Service = styled(Text)`
    color: #0a813d;
    font-size:14px;
`;
const Status = styled(Text)`
    color: #b00020;
    font-size:14px;
`;

