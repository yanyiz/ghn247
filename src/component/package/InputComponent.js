import React, {
    Component,
    Fragment,
    StrictMode,
} from 'react';
import {
    View,
    Button,
    Text,
    StyleSheet,
    ScrollView,

} from 'react-native';
import {
    Form, Item, Input, Label, Icon, Container, Content, Textarea, Picker
} from 'native-base';
import styled from 'styled-components';
//- create context
import ButtonComponent from './ButtonComponent'

class InputComponent extends Component {

    state = {
        reciverName: '',
        reciverTel: '',
        reciverAddr: '',
        deliveryAddr: '',
        cod: '',
        weight: '',
        area: '',
        selectType: '',
        note: ''
    }

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        // getPackage();
    }

    render() {
        return (
            <Fragment>
                {/* <ContainerView> */}
                <ScrollViewNew>
                    <Form>
                        <ItemForm rounded success >
                            <Icon name='person' style={{ color: '#4CAF50' }} />
                            <InputForm
                                placeholder={'Người nhận'}
                                onChangeText={(reciverName) => this.setState({ reciverName })}
                            />
                        </ItemForm>

                        <ItemForm rounded success >
                            <Icon name='md-call' style={{ color: '#4CAF50' }} />
                            <InputForm
                                placeholder={'Số Điện Thoại'}
                                onChangeText={(reciverTel) => this.setState({ reciverTel })}
                            />
                        </ItemForm>

                        <ItemForm rounded success >
                            <Icon name='md-map' style={{ color: '#4CAF50' }} />
                            <InputForm
                                placeholder={'Địa chỉ người nhận'}
                                onChangeText={(reciverAddr) => this.setState({ reciverAddr })}
                            />
                        </ItemForm>

                        <ItemForm rounded success >
                            <Icon name='md-pin' style={{ color: '#4CAF50' }} />
                            <InputForm
                                placeholder={'Nơi nhận'}
                                onChangeText={(deliveryAddr) => this.setState({ deliveryAddr })}
                            />
                        </ItemForm>

                        {/* <ItemForm rounded success >
                            <Icon name='md-list' style={{ color: '#4CAF50' }} />
                            <InputForm
                                placeholder={'Loại dịch vụ'}
                                onChangeText={(selectType) => this.setState({ selectType })}
                            />
                        </ItemForm> */}
                        <ItemForm rounded success >
                            <Icon name='md-list' style={{ color: '#4CAF50' }} />
                            <PickerView selectedValue={this.state.selectType} onValueChange={(selectType) => this.setState({ selectType })}>
                                <Picker.Item label="Cod" value="5add951cab91ae2a006cc1ef" />
                                <Picker.Item label="GH24" value="5add951cab91ae2a006cc1e1" />
                                <Picker.Item label="GH74" value="5add951cab91ae2a006cc1e2" />
                            </PickerView>
                        </ItemForm>

                        <ItemForm rounded success >
                            <Icon name='md-cash' style={{ color: '#4CAF50' }} />
                            <InputForm
                                placeholder={'Tiền COD'}
                                onChangeText={(cod) => this.setState({ cod })}
                            />
                        </ItemForm>

                        <ItemForm rounded success >
                            <Icon name='md-cube' style={{ color: '#4CAF50' }} />
                            <InputForm
                                placeholder={'Trọng lượng'}
                                onChangeText={(weight) => this.setState({ weight })}
                            />
                        </ItemForm>

                        <ItemForm rounded success >
                            <Icon name='md-locate' style={{ color: '#4CAF50' }} />
                            <InputForm
                                placeholder={'Khu vực'}
                                onChangeText={(area) => this.setState({ area })}
                            />
                        </ItemForm>
                        {/* <ItemForm rounded success > */}
                        <TextareaView rowSpan={5} bordered placeholder="Ghi Chú" />
                        {/* </ItemForm> */}
                    </Form>
                    {/* Button */}
                    <ButtonComponent
                        reciverName={this.state.reciverName}
                        reciverTel={this.state.reciverTel}
                        reciverAddr={this.state.reciverAddr}
                        deliveryAddr={this.state.deliveryAddr}
                        cod={this.state.cod}
                        weight={this.state.weight}
                        area={this.state.area}
                        selectType={this.state.selectType}
                        note={this.state.note}
                    />
                    <Padding />
                </ScrollViewNew>

                {/* </ContainerView> */}

            </Fragment>
        );
    }
}

export default InputComponent;

const ItemForm = styled(Item)`
    margin-bottom: 10px;
    font-size: 14px;
    shadow-color: #000;
    shadow-offset: { width: 1, height: 2 };
    shadow-opacity: 0.8;
    shadow-radius: 0;
    elevation: 2;
    border-color: #89C06A;
    border-width: 0.5;
    background-color:white;
`
const InputForm = styled(Input)`
    font-size: 14px;
`;

const ScrollViewNew = styled(ScrollView)`
    padding: 15px;

`;

const Padding = styled(View)`
    height:40px;
`;

const TextareaView = styled(Textarea)`
    margin-bottom: 10px;
    font-size: 14px;
    shadow-color: #000;
    shadow-offset: { width: 1, height: 2 };
    shadow-opacity: 0.8;
    shadow-radius: 0;
    elevation: 2;
    border-color: #89C06A;
    border-width: 0.5;
    background-color:white;
    border-radius: 25px;
`
const PickerView = styled(Picker)`
    
`
// margin-bottom: 10px;
//     font-size: 18px;
//     border-color: #89C06A;
//     border-width: 0.5;
// shadow-color: #000;
//     shadow-offset: { width: 1, height: 2 };
//     shadow-opacity: 0.8;
//     shadow-radius: 0;
//     border-color: #89C06A;
//     border-width: 0.5;
//     background-color:white;
//     border-radius: 25px;
//     elevation: 2;


getPackage = () => {
    fetch('http://54.199.238.117:3001/service/all', {
        method: 'get',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    }).then((response) => response.text())
        .then((responseText) => {
            let data = JSON.parse(responseText);
            alert(responseText);
        })
        .catch((error) => {
            console.error(error);
        });

}


