import React, {
  Component,
  Fragment
} from 'react';
import {
  View,
  Animated,
  TouchableHighlight,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native';
import { Text } from 'native-base';
import styled from 'styled-components';
import {Actions} from 'react-native-router-flux'
import { InputContext } from '../../context/LoginContext'

class ButtonComponent extends Component {

  constructor(props) {

    super(props)
    this.state = {
      pressStatus: false
    }

  }

  _onHideUnderlay() {
    this.setState({ pressStatus: false });
  }
  _onShowUnderlay() {
    this.setState({ pressStatus: true });
  }

  handlePress = () => {
      register(this.props.name, this.props.email, this.props.password)
  }
  render() {
    return (
      <Fragment>
        <Button onPress={this.handlePress}>
          <StyledView>Đăng ký</StyledView>
        </Button>
      </Fragment>
    );
  }
}

export default ButtonComponent;

const styles = StyleSheet.create({
  button: {
    borderColor: '#000066',
    borderWidth: 1,
    borderRadius: 10,
  },
  buttonPress: {
    borderColor: '#000066',
    // backgroundColor: '#000066',
    backgroundColor: 'black',
    borderWidth: 1,
    borderRadius: 10,
  },
})

register = (_name, _email, _password) => {
  // alert(_email, _password)
    fetch('http://54.199.238.117:3001/user/register', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: _name,
        email: _email,
        password: _password,
        // repassword: _repassword

      }),
    }).then((response) => response.text())
      .then((responseText) => {
        let data = JSON.parse(responseText);
        if(!data.data){
          Alert.alert(
            'Lỗi',
            data.message,
            [
              {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
          )
        }else{
          Alert.alert(
            'Thành công!!!',
            data.message,
            [
              {text: 'OK', onPress: () => Actions.login()},
            ],
            { cancelable: false }
          )
        }
        
      })
      .catch((error) => {
        console.error(error);
      });

}

// Css
const StyledView = styled.Text`
  font-size: 14px;
  text-align: center;
  color: white;
  padding-left:15px;
  padding-right:15px;
`;
const Button = styled.TouchableOpacity`
  background-color: #4CAF50;
  box-shadow: 0 5px 10px #000000;
  margin-top:5px;
  justify-content: center;
  height:45px;
  border-radius:25px;
  elevation: 2
`;