import React, {
    Component,
    Fragment,
    StrictMode,
} from 'react';
import {
    View,
    Button,
    Text,
    StyleSheet
} from 'react-native';
import {
    Form, Item, Input, Label, Icon
} from 'native-base';
import styled from 'styled-components';
//- create context
import { InputContext } from '../../context/LoginContext'
import ButtonComponent from './ButtonComponent'

class InputComponent extends Component {

    state = {
        name: '',
        email: '',
        password: '',
        repassword: '',
    }

    constructor(props) {
        super(props)
    }

    handlePress = () => {
        alert("123")
    }

    render() {
        return (
            <Fragment>
                <Form>
                    {/* name */}
                    <ItemForm rounded success >
                        <Icon name='person' style={{ color: '#4CAF50' }} />
                        <InputForm
                            placeholder={'Họ Tên'}
                            onChangeText={(name) => this.setState({ name })}
                        />
                    </ItemForm>

                    {/* email */}
                    <ItemForm rounded success >
                        <Icon name='mail' style={{ color: '#4CAF50' }} />
                        <InputForm
                             placeholder={'Email'}
                             onChangeText={(email) => this.setState({ email })}
                        />
                    </ItemForm>

                    {/* password */}
                    <ItemForm rounded success >
                        <Icon name='lock' style={{ color: '#4CAF50' }} />
                        <InputForm
                              placeholder={'Mật khẩu'}
                              onChangeText={(password) => this.setState({ password })}
                              secureTextEntry
                        />
                    </ItemForm>

                    {/* repassword */}
                    <ItemForm rounded success >
                        <Icon name='lock' style={{ color: '#4CAF50' }} />
                        <InputForm
                              placeholder={'Nhập lại mật khẩu'}
                              onChangeText={(repassword) => this.setState({ repassword })}
                              secureTextEntry
                        />
                    </ItemForm>
                </Form>
                
                {/* Button */}
                <ButtonComponent name={this.state.name} repassword={this.state.repassword} email={this.state.email} password={this.state.password} />
            </Fragment>
        );
    }
}

export default InputComponent;

const ItemForm = styled(Item)`
    margin-bottom: 10px;
    font-size: 14px;
    shadow-color: #000;
    shadow-offset: { width: 1, height: 2 };
    shadow-opacity: 0.8;
    shadow-radius: 0;
    elevation: 2;
    borderColor: #89C06A;
    borderWidth: 0.5;
    background-color:white;
`
const InputForm = styled(Input)`
    font-size: 14px;
`;